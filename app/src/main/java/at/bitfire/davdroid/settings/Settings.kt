package at.bitfire.davdroid.settings

object Settings {

    const val DISTRUST_SYSTEM_CERTIFICATES = "distrust_system_certs"

    const val OVERRIDE_PROXY = "override_proxy"
    const val OVERRIDE_PROXY_HOST = "override_proxy_host"
    const val OVERRIDE_PROXY_PORT = "override_proxy_port"

}
